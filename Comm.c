/* 
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 作者              日期      
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 魏彬       		2014年12月26日	
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 描述
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 血氧与血压通讯处理函数，主要是一个状态机
 * 修改：发送血压计数据与OLED显示数据同步，kebin，20150721
 *       增加接收实时压力值功能，并转至显示界面：（未加）
            SPO2orBP_Bef=SPO2orBP;
            SPO2orBP = 2;	 
 ***********************************************************************/


#include "p32mx150f128b.h"
#include "stdio.h"
#include "extern_func.h"
#include "Port_init.h"
#include <plib.h>

/*//状态机值
#define I2C_FSM_ERROR 0xFF
#define I2C_FSM_IDLE 0 
#define I2C_FSM_WRITE 1 
#define I2C_FSM_WRITE_DATA 2 
#define I2C_FSM_WRITE_DATA2 3 
#define I2C_FSM_WRITE_DATA3 4
#define I2C_FSM_WRITE_TIME 5
#define I2C_FSM_WRITE_CONFIG 6
#define I2C_FSM_READ 7		 
#define I2C_FSM_END 8
*/
//状态机值
#define I2C_FSM_ERROR 0xFF
#define I2C_FSM_IDLE 0 
#define I2C_FSM_WRITE 1 
#define I2C_FSM_WRITE_DATA 2 
#define I2C_FSM_WRITE_DATA2 3 
#define I2C_FSM_WRITE_DATA3 4
#define I2C_FSM_WRITE_TIME 5
#define I2C_FSM_WRITE_CONFIG 6
#define I2C_FSM_WRITE_PRESSURE 7
#define I2C_FSM_READ 8		 
#define I2C_FSM_END 9

//定义一个共同体，用于数据保存和发送

typedef union {
    unsigned char char_data[2];
    unsigned short int Sint_data;
} HiHg;

unsigned short int SintWave = 0;
unsigned int HeadFlag = 0;
unsigned char LowmmHg = 0;
unsigned short int HimmHg = 0;
//unsigned short int mmHg = 0;   //--pan delete
unsigned  int mmHg = 0;
unsigned int HighmmHgcnt = 0;
unsigned int LowFlag = 0;
unsigned int HighFlag = 0;
unsigned int TimeFlag = 0;
unsigned int ConfigFlag = 0;
unsigned int TimeCnt = 0;
unsigned char I2CAddress = 0;
unsigned char I2CDate = 0;
unsigned int I2C_state = I2C_FSM_IDLE;
unsigned char TimeArr[8];
unsigned int AllDataCnt = 0;
unsigned int WaveDataCnt = 0;
unsigned int SpO2DataCnt = 0;
unsigned int AllDataFlag = 0;
unsigned int WaveDataFlag = 0;
unsigned int SpO2DataFlag = 1;
unsigned char AllData[7] = {0x55, 0xF5, 0x00, 0x00, 0x00, 0x00, 0x00}; //[2]脉率，[3]SPO2,[4],[5]波形，[6]校验码
unsigned char WaveData[5] = {0x55, 0xF3, 0x00, 0x00, 0x00}; //[2],[3]为波形，[4]为校验码
unsigned char SpO2Data[5] = {0x55, 0xF4, 0x00, 0x00, 0x00}; //[2]为脉率,[3]为血氧，[4]为校验码
unsigned char EndData = 0x58; //结束标志位
HiHg HighmmHg;
HiHg WaveUnion;
unsigned int PRFlag = 0;
unsigned char PR_BP = 0;
unsigned char NewBPdata = 0;
unsigned char ReadI2C2Flag = 0;
unsigned char Pressurecnt = 0;
unsigned int Pressuredata[2] = {0};  //保存实时压力变量
/*unsigned int pressure_record[200] = {0};   //测试保存实时压力值
unsigned char pressure_cnt = 0;*/

/*
错误复位函数，当状态机出错时，将所有状态和变量复位
 */
void ErrRst() {
    unsigned char tempI2CData = 0;
    I2C2CONbits.SCLREL = 1; //释放SCL
    ReadI2C2Flag = 0;
    AllDataCnt = 0;
    WaveDataCnt = 0;
    SpO2DataCnt = 0;
    AllDataFlag = 0;
    WaveDataFlag = 0;
    SpO2DataFlag = 1;
    HeadFlag = 0;
    //LowmmHg = 0;  xkb
    HighmmHgcnt = 0;
    LowFlag = 0;
    HighFlag = 0;
    TimeFlag = 0;
    TimeCnt = 0;
    ConfigFlag = 0;
    PRFlag = 0;
    //PR_BP = 0; xkb
    //HighmmHg.Sint_data = 0;xkb
//实时压力接收
	Pressuredata[0] = 0;  //实时压力值清零
	Pressuredata[1] = 0;  //实时压力值清零
	Pressurecnt = 0;      //保证下一次接收到正确的实时压力
    WaveUnion.Sint_data = 0;
    while (I2C2STATbits.RBF)
        tempI2CData = I2C2RCV;
    while (I2C2STATbits.TBF);
}

//I2C2STATbits.R_W;	//1:read 0:write	
//I2C2STATbits.D_A;   //1:Date 0:address
//I2C2STATbits.RBF;	//1:i2c2rcv full(recieve finished)
//I2C2STATbits.TBF;	//1:i2c2trn full(transimitting)
//I2C2CONbits.SCLREL = 1 //1:释放SCL，0:SCL拉低

/*
总体处理函数
 */
void Comm_Process() {
    //最开始的这部分，主要用于事先判断是地址数据还是信息数据
    unsigned char I2C2DATA_test = 0;
    if (I2C2STATbits.I2COV) //当检测到总线错误时，读空fifo，并清零I2COV，进入IDLE状态
    {
        while (I2C2STATbits.RBF)
            I2C2DATA_test = I2C2RCV;
        I2C2STATbits.I2COV = 0;
        I2C_state = I2C_FSM_IDLE;
        ErrRst();
    } else if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 0)) //检测到wrtie地址
    {
        I2C_state = I2C_FSM_WRITE;
        I2CDate = I2C2RCV; //地址数据也需要读空
        ReadI2C2Flag = 0; //read标志位清零
    } else if ((I2C2STATbits.R_W == 1)&&(I2C2STATbits.D_A == 0)) //检测到read地址
    {
        ReadI2C2Flag = 1; //read标志位置位
        if (BefSleepFlag) //判断是否休眠前，如果休眠前，则进入END状态
        {
            I2C_state = I2C_FSM_END;
        } else if (BefSleepFlag == 0) //否则进入正常READ状态
        {
            I2C_state = I2C_FSM_READ;
        }
        I2CDate = I2C2RCV; //读空fifo
        I2C2TRN = 0xAA; //发送0xAA（协议中规定）
        I2C2CONbits.SCLREL = 1; //释放SCL
        AllDataCnt = 0;
        WaveDataCnt = 0;
        SpO2DataCnt = 0;
    }
    else //否则进入正常读写状态机						
    {
        switch (I2C_state) {
            case I2C_FSM_IDLE: //IDLE状态下，从读地址或写地址开始
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 0)) {
                    I2C_state = I2C_FSM_WRITE;
                    ReadI2C2Flag = 0;
                } else if ((I2C2STATbits.R_W == 1)&&(I2C2STATbits.D_A == 0)) {
                    I2C_state = I2C_FSM_READ;
                    ReadI2C2Flag = 1;
                    I2C2TRN = 0xAA;
                    I2C2CONbits.SCLREL = 1; //释放SCL	
                    AllDataCnt = 0;
                    WaveDataCnt = 0;
                    SpO2DataCnt = 0;
                } else {
                    I2C_state = I2C_FSM_IDLE;
                    I2C2CONbits.SCLREL = 1; //释放SCL
                    ReadI2C2Flag = 0;
                }
				Pressuredata[0] = 0;  //实时压力值清零
				Pressuredata[1] = 0;  //实时压力值清零
				Pressurecnt = 0;      //保证下一次接收到正确的实时压力
                break;
            case I2C_FSM_WRITE:
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) //检测到write数据
                {
                    if (HeadFlag) //检测到头以后，接下去进行其他功能指令判断
                    {
						if(I2CDate == 0xCC)			//接收实时压力值
						{
							I2C_state = I2C_FSM_WRITE_PRESSURE;
							HeadFlag = 0;
						}
                        else if (I2CDate == 0xFF) //接收血压/脉率值
                        {
                            I2C_state = I2C_FSM_WRITE_DATA;
                            HeadFlag = 0;
                        }
                        else if (I2CDate == 0xEE) //接收基准时间
                        {
                            I2C_state = I2C_FSM_WRITE_TIME;
                            HeadFlag = 0;
                        } else if (I2CDate == 0xDD) //接收配置字
                        {
                            I2C_state = I2C_FSM_WRITE_CONFIG;
                            HeadFlag = 0;
                        } else {
                            I2C_state = I2C_FSM_ERROR;
                            ErrRst();
                        }
                    } else if (I2CDate == 0xAA) //检测到头
                    {
                        HeadFlag = 1;
                        I2C_state = I2C_FSM_WRITE;
                    } else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                I2C2CONbits.SCLREL = 1; //释放SCL		//加上会安全一点，如果RBF满了，会自动拉低，需要手动释放
                break; //在使能时钟延时位置位时
            case I2C_FSM_WRITE_DATA:
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) {
                    if (LowFlag) {
                        LowmmHg = I2CDate; //舒张压数据
                        I2C_state = I2C_FSM_WRITE_DATA2; //进入接收收缩压状态机
                        LowFlag = 0;
                    } else if (I2CDate == 0xF1) //舒张压标志位
                    {
                        LowFlag = 1;
                        I2C_state = I2C_FSM_WRITE_DATA;
                    } else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                break;
            case I2C_FSM_WRITE_DATA2: //接收收缩压状态机
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) {
                    if (HighFlag) {
                        if (HighmmHgcnt == 1) //接收高字节
                        {
                            HighmmHg.char_data[1] = I2CDate;
                            HimmHg = HighmmHg.Sint_data;
                            I2C_state = I2C_FSM_WRITE_DATA3; //进入脉率数据接收状态机
                            HighmmHgcnt = 0;
                            HighFlag = 0;
                        } else if (HighmmHgcnt == 0) //先接收低字节
                        {
                            HighmmHg.char_data[0] = I2CDate;
                            I2C_state = I2C_FSM_WRITE_DATA2; //仍进入收缩压接收状态机
                            HighmmHgcnt = 1;
                        } else {
                            I2C_state = I2C_FSM_ERROR;
                            ErrRst();
                        }
                    } else if (I2CDate == 0xF2) //收缩压标志位
                    {
                        HighFlag = 1;
                        HighmmHgcnt = 0;
                        I2C_state = I2C_FSM_WRITE_DATA2;
                    } else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                break;

            case I2C_FSM_WRITE_DATA3: //接收脉率状态机
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) {
                    if (PRFlag) {
                        PR_BP = I2CDate; //脉率数据
                        I2C_state = I2C_FSM_IDLE;
                        PRFlag = 0;
                        SPO2orBP_Bef = SPO2orBP;
                        SPO2orBP = 1; //转至血压显示
                    } else if (I2CDate == 0xF3) //脉率标志位
                    {
                        PRFlag = 1;
                        I2C_state = I2C_FSM_WRITE_DATA3;
                    } else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                break;

			case I2C_FSM_WRITE_PRESSURE:				//接收实时压力值
				I2CDate = I2C2RCV;
				if((I2C2STATbits.R_W==0)&&(I2C2STATbits.D_A==1))
				{	
					if(Pressurecnt == 1)
					{
						Pressuredata[1] = I2CDate & 0x000000ff;    //高位实时压力值

						SPO2orBP_Bef = SPO2orBP;
						SPO2orBP = 2;                 //显示实时压力值标志位置2
						mmHg = (Pressuredata[1] <<8) + Pressuredata[0];
						I2C_state = I2C_FSM_IDLE;
						Pressurecnt = 0;
					}
					else if(Pressurecnt == 0)
					{///pressure_cnt ++;
						Pressuredata[0] = I2CDate & 0x000000ff;    //低位实时压力值
						///pressure_record[pressure_cnt] = I2CDate & 0x000000ff;
						I2C_state = I2C_FSM_WRITE_PRESSURE;//仍进入接收实时压力值状态机
						Pressurecnt = 1;
					}
					else
					{
						I2C_state = I2C_FSM_ERROR;
						ErrRst();
					}
/*if(pressure_cnt<=200)
{
					pressure_record[pressure_cnt] = I2CDate & 0x000000ff;
}*/

				}
				else
				{
					I2C_state = I2C_FSM_ERROR;
					ErrRst();
				}
				break;

            case I2C_FSM_WRITE_TIME: //接收标准时间状态机
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) {
                    if (TimeFlag) {
                        TimeArr[TimeCnt] = I2CDate; //接收8个字节的时间数据
                        TimeCnt++; //年月日时分秒毫秒
                        if (TimeCnt >= 8) {
                            TimeFlag = 0;
                            TimeCnt = 0;
                            I2C_state = I2C_FSM_IDLE;
                        } else {
                            I2C_state = I2C_FSM_WRITE_TIME;
                        }
                    } else if (I2CDate == 0xF8) //基准时间头
                    {
                        TimeFlag = 1;
                        TimeCnt = 0;
                        I2C_state = I2C_FSM_WRITE_TIME;
                    } else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                break;

            case I2C_FSM_WRITE_CONFIG: //进入接收配置字状态机
                I2CDate = I2C2RCV;
                if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) {
                    if (ConfigFlag) {
                        ConfigFlag = 0;
                        if (I2CDate == 0xE1) //slave write时，发送wave波形
                        {
                            WaveDataFlag = 1;
                            AllDataFlag = 0;
                            SpO2DataFlag = 0;
                            I2C_state = I2C_FSM_IDLE;
                        } else if (I2CDate == 0xE2) //slave write时，发送血氧/脉率数据
                        {
                            WaveDataFlag = 0;
                            SpO2DataFlag = 1;
                            AllDataFlag = 0;
                            I2C_state = I2C_FSM_IDLE;
                        } else if (I2CDate == 0xE3) //slave write时，发送所有数据（波形和血氧/脉率）
                        {
                            WaveDataFlag = 0;
                            AllDataFlag = 1;
                            SpO2DataFlag = 0;
                            I2C_state = I2C_FSM_IDLE;
                        } else {
                            WaveDataFlag = 0;
                            AllDataFlag = 0;
                            SpO2DataFlag = 1;
                            I2C_state = I2C_FSM_ERROR;
                            ErrRst();
                        }
                    } else if (I2CDate == 0xF0) //配置字头
                    {
                        ConfigFlag = 1;
                        I2C_state = I2C_FSM_WRITE_CONFIG;
                    } else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                break;

            case I2C_FSM_READ: //进入slave write状态机
                if ((I2C2STATbits.R_W == 1)&&(I2C2STATbits.D_A == 1)) {
                    if (AllDataFlag) //根据配置字的接收值，进入对应状态
                    {
                        WaveDataFlag = 0;
                        WaveDataCnt = 0;
                        SpO2DataFlag = 0;
                        SpO2DataCnt = 0;
                        if (AllDataCnt >= 2 && AllDataCnt <= 5) //发送主体数据部分
                        {
                            while (I2C2STATbits.TBF);
                            I2C2TRN = AllData[AllDataCnt];
                            AllData[6] = AllData[6] + AllData[AllDataCnt];
                            I2C_state = I2C_FSM_READ;
                            AllDataCnt++;
                        } else if (AllDataCnt <= 1) //发送前2个字节
                        {
                            if (AllDataCnt == 0)
                                AllData[6] = 0;
                            while (I2C2STATbits.TBF);
                            I2C2TRN = AllData[AllDataCnt];
                            AllData[6] = AllData[6] + AllData[AllDataCnt]; //最后一个字节为校验和
                            I2C_state = I2C_FSM_READ;
                            if (AllDataCnt == 1) {
                                WaveUnion.Sint_data = IR >> 3;
                                AllData[2] = Pulse_rate_OLED_display;
                                AllData[3] = SPO2_OLED_display;
                                AllData[4] = WaveUnion.char_data[0];
                                AllData[5] = WaveUnion.char_data[1];
                            }
                            AllDataCnt++;
                        }
                        else if (AllDataCnt == 6) //发送校验和
                        {
                            AllDataCnt = 0;
                            while (I2C2STATbits.TBF);
                            I2C2TRN = AllData[6];
                            AllData[6] = 0;
                            I2C_state = I2C_FSM_IDLE;
                            ReadI2C2Flag = 0; //发数结束
                        } else {
                            I2C_state = I2C_FSM_ERROR;
                            ErrRst();
                        }
                    } else if (WaveDataFlag) //发送波形数据
                    {
                        AllDataFlag = 0;
                        AllDataCnt = 0;
                        SpO2DataFlag = 0;
                        SpO2DataCnt = 0;
                        if (WaveDataCnt == 2 || WaveDataCnt == 3) //发送主体数据
                        {
                            while (I2C2STATbits.TBF);
                            I2C2TRN = WaveData[WaveDataCnt];
                            WaveData[4] = WaveData[4] + WaveData[WaveDataCnt];
                            I2C_state = I2C_FSM_READ;
                            WaveDataCnt++;
                        } else if (WaveDataCnt <= 1) //发送标志字节
                        {
                            if (WaveDataCnt == 0)
                                WaveData[4] = 0;
                            while (I2C2STATbits.TBF);
                            I2C2TRN = WaveData[WaveDataCnt];
                            WaveData[4] = WaveData[4] + WaveData[WaveDataCnt];
                            I2C_state = I2C_FSM_READ;
                            if (WaveDataCnt == 1) {
                                WaveUnion.Sint_data = IR >> 3;
                                WaveData[2] = WaveUnion.char_data[0];
                                WaveData[3] = WaveUnion.char_data[1];
                            }
                            WaveDataCnt++;
                        }
                        else if (WaveDataCnt == 4) //发送校验和
                        {
                            WaveDataCnt = 0;
                            while (I2C2STATbits.TBF);
                            I2C2TRN = WaveData[4];
                            WaveData[4] = 0;
                            I2C_state = I2C_FSM_IDLE;
                            ReadI2C2Flag = 0;
                        } else {
                            I2C_state = I2C_FSM_ERROR;
                            ErrRst();
                        }
                    }
                    else if (SpO2DataFlag) //发送血氧/脉率数据
                    {
                        AllDataFlag = 0;
                        AllDataCnt = 0;
                        WaveDataFlag = 0;
                        WaveDataCnt = 0;
                        if (SpO2DataCnt == 2 || SpO2DataCnt == 3) //主体数据
                        {
                            while (I2C2STATbits.TBF);
                            I2C2TRN = SpO2Data[SpO2DataCnt];   //发送脉率、血氧的显示值
                            SpO2Data[4] = SpO2Data[4] + SpO2Data[SpO2DataCnt];  //计算校验和
                            I2C_state = I2C_FSM_READ;
                            SpO2DataCnt++;
                        } else if (SpO2DataCnt <= 1) {
                            if (SpO2DataCnt == 0)
                                SpO2Data[4] = 0;
                            while (I2C2STATbits.TBF);
                            I2C2TRN = SpO2Data[SpO2DataCnt];   //发送0x55 0xF4
                            SpO2Data[4] = SpO2Data[4] + SpO2Data[SpO2DataCnt];  //计算校验和
                            I2C_state = I2C_FSM_READ;
                            if (SpO2DataCnt == 1) {
                                SpO2Data[2] = Pulse_rate_OLED_display;
                                SpO2Data[3] = SPO2_OLED_display;
                            }
                            SpO2DataCnt++;
                        }
                        else if (SpO2DataCnt == 4) //校验和
                        {
                            SpO2DataCnt = 0;
                            while (I2C2STATbits.TBF);
                            I2C2TRN = SpO2Data[4];
                            SpO2Data[4] = 0;
                            I2C_state = I2C_FSM_IDLE;
                            ReadI2C2Flag = 0;
                        } else {
                            I2C_state = I2C_FSM_ERROR;
                            ErrRst();
                        }
                    }
                    else {
                        I2C_state = I2C_FSM_ERROR;
                        ErrRst();
                    }
                } else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                I2C2CONbits.SCLREL = 1; //释放SCL
                break;
            case I2C_FSM_END: //结束状态机，在进入休眠前，回数
                if ((I2C2STATbits.R_W == 1)&&(I2C2STATbits.D_A == 1)) {
                    WaveDataFlag = 0;
                    WaveDataCnt = 0;
                    SpO2DataFlag = 0;
                    SpO2DataCnt = 0;
                    AllDataFlag = 0;
                    AllDataCnt = 0;
                    while (I2C2STATbits.TBF);
                    I2C2TRN = EndData; //0x58
                    I2C_state = I2C_FSM_IDLE;
                    ReadI2C2Flag = 0;
                }
                else {
                    I2C_state = I2C_FSM_ERROR;
                    ErrRst();
                }
                I2C2CONbits.SCLREL = 1; //释放SCL
                break;
            case I2C_FSM_ERROR:
                ErrRst();
                I2C_state = I2C_FSM_IDLE;
                break;
        }
    }
}



/*用来测试单栋发过来的实时压力数据是否正确

void Comm_Process1() 
{
    //最开始的这部分，主要用于事先判断是地址数据还是信息数据
    unsigned char I2C2DATA_test = 0;
    if (I2C2STATbits.I2COV) //当检测到总线错误时，读空fifo，并清零I2COV，进入IDLE状态
    {
        while (I2C2STATbits.RBF)
            I2C2DATA_test = I2C2RCV;
        I2C2STATbits.I2COV = 0;
        I2C_state = I2C_FSM_IDLE;
        ErrRst();
    } else if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 0)) //检测到wrtie地址
    {
        I2C_state = I2C_FSM_WRITE;
        I2CDate = I2C2RCV; //地址数据也需要读空
        ReadI2C2Flag = 0; //read标志位清零
    } else if ((I2C2STATbits.R_W == 1)&&(I2C2STATbits.D_A == 0)) //检测到read地址
    {
        ReadI2C2Flag = 1; //read标志位置位
        if (BefSleepFlag) //判断是否休眠前，如果休眠前，则进入END状态
        {
            I2C_state = I2C_FSM_END;
        } else if (BefSleepFlag == 0) //否则进入正常READ状态
        {
            I2C_state = I2C_FSM_READ;
        }
        I2CDate = I2C2RCV; //读空fifo
        I2C2TRN = 0xAA; //发送0xAA（协议中规定）
        I2C2CONbits.SCLREL = 1; //释放SCL
        AllDataCnt = 0;
        WaveDataCnt = 0;
        SpO2DataCnt = 0;
    }
 else //否则进入正常读写状态机						
    {
        switch (I2C_state) 
				{
				   ///I2CDate = I2C2RCV;
					///if ((I2C2STATbits.R_W == 0)&&(I2C2STATbits.D_A == 1)) {
           			case I2C_FSM_WRITE: //结束状态机，在进入休眠前，回数
						pressure_cnt ++;
						if(pressure_cnt <= 200)
						{
						pressure_record[pressure_cnt] = I2C2RCV;
						}
                		I2C_state = I2C_FSM_WRITE;
				///	}
				break;
	}
}
}
*/