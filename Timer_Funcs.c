/* 
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 作者              日期      
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 魏彬       		2014年5月22日	
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 描述
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Timer初始化。本项目主要用了T3和T4
 **********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include <plib.h>
#include "Port_init.h"

void initTimer1();
void initTimer2();
void initTimer3();
void initTimer4();

/****************************************
函数名：initTimer1()
参数：
描述：初始化Timer1,该时钟为外部32k时钟，用于RTCC
 **************************************/

void initTimer1() {
    T1CON = 0x0; // 禁止T1先
    T1CONbits.TCS = 1; //使用内部时钟
    //	T1CONbits.TCKPS = 2;	// 1:64的预分频
    T1CONbits.TSYNC = 1;
    TMR1 = 0x0; // 清零计数器

    IEC0bits.T1IE = 0;
    T1CONSET = 0x8000; // 使能T1

    /*
        IPC1bits.T1IP = 5; 		// 设置中断优先级	//详见总体datasheet 表7.1
        IPC1bits.T1IS = 1; 		// 
        IFS0bits.T1IF = 0; 		// 清T4中断标志位 
        IEC0bits.T1IE = 1; 		// 使能T4中断

        T1CON = 0x0;           	// 禁止T4先
        T1CONbits.TCS = 0;		//使用内部时钟
        T1CONbits.TCKPS = 3;	// 1:64的预分频   2
        TMR1 = 0x0; 			// 清零计数器
        PR1 = 31250;				//16M/64/1250=200Hz   1250 
        T1CONSET = 0x8000; 		// 使能T4
     */
}

/****************************************
函数名：initTimer2()
参数：
描述：初始化Timer2,该时钟为32位时钟，用于光频转换输出信号的检测
 **************************************/
void initTimer2() {
    T2CON = 0x0;
    T3CON = 0x0;
    TMR2 = 0;
    PR2 = 0xFFFFFFFF;

    T2CONbits.T32 = 1; //32bit时钟
    T2CONbits.TCS = 0; // 采用内部时钟32MHz/2
    T2CONbits.SIDL = 1; //空闲时停止工作
    T2CONbits.TCKPS = 0; // 预分频1:1
    T2CONbits.TGATE = 0;
    T2CONSET = 0x8000; // 开始计数
}

/****************************************
函数名：initTimer3()
参数：
描述：初始化Timer3,该时钟为16位时钟，用于光频转换输出信号的检测
 **************************************/
void initTimer3() {
    IPC3bits.T3IP = 3; // 设置中断优先级	//详见总体datasheet 表7.1
    IPC3bits.T3IS = 1; // 
    IFS0bits.T3IF = 0; // 清T3中断标志位 
    IEC0bits.T3IE = 1; // 使能T3中断

    T3CON = 0x0;
    TMR3 = 0;
    PR3 = 0xF9F; //1000Hz产生，即1ms计数
    T3CONbits.TCS = 0; // 采用内部时钟4MHz
    T3CONbits.TCKPS = 0; // 预分频1:1
    T3CONbits.TGATE = 0;
    //T3CONSET = 0x8000; 		// 开始计数
}

/****************************************
函数名：initTimer4()
参数：
描述：初始化Timer4,该时钟采用外部8MHz为基准，用于对LED的周期性调制
内部时钟设置为：8MHz——》4MHz——》PB 4M         64MHz(pll clk)——》32MHz(sysclk)——》16MHz(pd_clk)
 **************************************/
void initTimer4() {
    //IPC4bits.T4IP = 2; 		// 设置中断优先级	//详见总体datasheet 表7.1
    IPC4bits.T4IP = 1; //pan
    IPC4bits.T4IS = 1; //
    T4CON = 0x0; // 禁止T4先
    T4CONbits.TCS = 0; //使用内部时钟
    T4CONbits.TCKPS = 6; // 6:1:64的预分频(20Hz)/5:  1:32的预分频(40Hz)     
    TMR4 = 0x0; // 清零计数器
    PR4 = 3125; //16M/64/1250=200Hz   1250
    IFS0bits.T4IF = 0; // 清T4中断标志位 
    IEC0bits.T4IE = 0; // 使能T4中断
    // 使能T4
    T4CONbits.ON = 1;

}