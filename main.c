/* 
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 作者              日期      
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 魏彬       		2014年5月22日	
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 描述
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 最终版，针对PIC32MX150F128B芯片进行程序的重新修改
 *************************检验修改**************************************
 * 修改人：邢科斌
 * 时间：2015年7月31日
 * 内容：
 * R曲线：统一采用Nellcor曲线，适用于Fluke、Oxitest Plus7血氧模拟仪
 *          //tSpO2 = -12.39*ratio*ratio-18.47*ratio+111.9;  //7.29N-20,20p,RED+IR,SENSOR:#059
 *          tSpO2 = 120.7 -40.04 * ratio; //7.29 N-20,20p,RED+IR,SENSOR:#059
 * Fluke血氧模拟仪还需更改：
 * 1、光强：AFE44x0_Reg_Write((unsigned char)LEDCNTRL, 0x016680);  //LED1=20mA;LED2=25mA;(fluke)
 * 2、high_cnt>=5、low_cnt>=5改为high_cnt>=4、low_cnt>=4，将在fluke下高脉率只能到160扩展到245
 * 3、修改unsigned int FilterHP(int sample)函数中高通滤波器的特性：
 *      #define HP_f 63  //31  
 *      #define HP_f_bit 6//5
 *      unsigned int Pulse_HP_buffer[64];//32
 * 即将缓存的长度从32改为64，将在fluke下低脉率只能到50扩展到30
 * 正常产品化时只需更改R曲线即可。
 * 
 * * 修改人：邢科斌
 * 时间：2016年3月7日
 * 内容：
 * 修改了高低通滤波器，加入一阶五点差分器
 * 增加了基线漂移的识别,波形一直保持显示状态；
 * 去除了陀螺仪用于运动识别的功能，只用于识别方向
 * 脉搏波最值自适应阈值
 * 更改了整套脉率和血氧的计算算法
 * 重新标定了R曲线（Nellcor）：tSpO2 = 122 -39.96 * ratio;
 ***********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include "Port_init.h"
#include "AFE44x0.h"
#include <stdio.h>
#include <plib.h>
#include <math.h>
#include <p32xxxx.h>


#define FSM_IDLE  0x00
#define FSM_MOTION1 0x01
#define FSM_MOTION2 0x02

#define Const3  0//400000000//3000000 	
#define Desampling_rate 4
#define LP_f1  5      //原始采集数据低通滤波器阶数

#define HP_f 15 //weibin
//脉搏波滤波
#define LP_f_bit 3
#define LP_f     8
#define LP_f_orders 1      //低通阶数

#define HP_f_bit 5
#define HP_f_orders 6   //高通阶数

//原始数据滤波
#define LP_IR_RD_f_bit 3
#define LP_IR_RD_f     8
#define LP_IR_RD_f_orders 1      //低通阶数

#define HP_IR_RD_f_bit 5
#define HP_IR_RD_f_orders 6    //高通阶数



#define Hz_upper_threshold    0.9			//光强阈值
#define Hz_lower_threshold    0.01			//光弱阈值
#define Min_Value_threshold   0.3           //脉搏波最小值阈值系数   
#define Max_Value_threshold   0.3           //脉搏波最大值阈值系数
#define Min_PPG_Gap   450//模拟仪450                   //波谷最小值发生突变量的阈值
#define Min_PPG_Gap_threshold  0.4//0.2         //识别波谷最小值发生突变量的阈值系数
#define MinPPGValueGapCntMax   20                //波形突变次数
#define no_finger_threshold   400			//差不多8秒
#define Pulse_buffer_size   300	
#define Pulse_Index_Start   (400)           //400个数据以后滤波的数据才稳定


//RTCC
#define InitTime     0x08000000		//8:00
#define InitDate     0x14010103		//2014年1月1日 星期三

#define sampleRate 50

//函数声明
unsigned int Pulse_rate_fn(); //用于脉率及SpO2计算	
void filter4(); //4点平滑滤波
void ModelSelect(); //发光测量模式选择
void avg_fun();
void Bef_GotoSleep();
void SportFSM(); //运动状态机
void SpO2_Pulse_Display_avg_fun(void);
void send_uart(void);
unsigned int FilterHP(int);
int FilterHP1(int);
//BG/IR/RD强度计算				
int decRD_signal = 0;
int decIR_signal = 0;
int decRD_raw = 0;
int decIR_raw = 0;
int decRD_A = 0;
int decIR_A = 0;

int RD_Intensity = 0;
int IR_Intensity = 0;
//用于4点平滑滤波
unsigned char Desampling = 0;
int RD = 0; //WEB			
int RD_temp3 = 0;
int RD_temp2 = 0;
int RD_temp1 = 0;
int IR = 0; //WEB
int IR_temp3 = 0;
int IR_temp2 = 0;
int IR_temp1 = 0;

unsigned int Pulse_rate_LP = 0;
unsigned int Pulse_rate_temp3 = 0;
unsigned int Pulse_rate_temp2 = 0;
unsigned int Pulse_rate_temp1 = 0;

unsigned int SpO2_temp1 = 0;
unsigned int SpO2_temp2 = 0;
unsigned int SpO2_temp3 = 0;
unsigned int SpO2_LP = 0;
//pan
unsigned int SpO2_temp7 = 0;
unsigned int SpO2_temp6 = 0;
unsigned int SpO2_temp5 = 0;
unsigned int SpO2_temp4 = 0;
//pan
unsigned int Pulse_rate_temp7 = 0;
unsigned int Pulse_rate_temp6 = 0;
unsigned int Pulse_rate_temp5 = 0;
unsigned int Pulse_rate_temp4 = 0;

unsigned char WaveIdx = 0;
unsigned char SPO2_OLED_display; //用于血氧显示
unsigned char Pulse_rate_OLED_display; //用于脉率血氧显示 
unsigned char OLEDSPO2_cnt = 0; //8次显示滤波缓冲
unsigned char LED_Display_Timer4En = 0; //定时器4显示使能
//血氧显示值缓存
unsigned char SPO2_OLED_temp1 = 0;
unsigned char SPO2_OLED_temp2 = 0;
unsigned char SPO2_OLED_temp3 = 0;
unsigned char SPO2_OLED_temp4 = 0;
unsigned char SPO2_OLED_temp5 = 0;
unsigned char SPO2_OLED_temp6 = 0;
unsigned char SPO2_OLED_temp7 = 0;
//脉率显示值缓存
unsigned char Pulse_rate_OLED_temp1 = 0;
unsigned char Pulse_rate_OLED_temp2 = 0;
unsigned char Pulse_rate_OLED_temp3 = 0;
unsigned char Pulse_rate_OLED_temp4 = 0;
unsigned char Pulse_rate_OLED_temp5 = 0;
unsigned char Pulse_rate_OLED_temp6 = 0;
unsigned char Pulse_rate_OLED_temp7 = 0;

//原始数据进行高低通滤波
unsigned char IR_RD_HLP = 1;

int IR_LP_temp = 0;
int IR_LP_pass[LP_f_bit + 1] = {0};
int IR_LP_out[LP_f_orders] = {0};
int IR_LP = 0;

int RD_LP_temp = 0;
int RD_LP_pass[LP_f_bit + 1] = {0};
int RD_LP_out[LP_f_orders] = {0};
int RD_LP = 0;


//进行高低通运算
unsigned char Pulse_HLP = 1;
int Pulse_HP_templ = 0;
unsigned int Pulse_HP = 0;
unsigned int Pulse_HP_buffer[256] = {0};
unsigned char HB_counter = 0;
int Pulse_HP_pass[HP_f_orders + 1] = {0};
int Pulse_HP_out[HP_f_orders] = {0};
int Pulse_LP_temp = 0;
int Pulse_LP_pass[LP_f_bit + 1] = {0};
int Pulse_LP_out[LP_f_orders] = {0};

unsigned int Pulse_HP_templ2 = 0;
unsigned int Pulse_HP2 = 0;
unsigned int Pulse_HP_buffer2[256] = {0};
unsigned char HB_counter2 = 0;

unsigned int Pulse_HP_templ3 = 0;
unsigned int Pulse_HP3 = 0;
unsigned int Pulse_HP_buffer3[256] = {0};
unsigned char HB_counter3 = 0;

unsigned int Pulse_HP_templ4 = 0;
unsigned int Pulse_HP4 = 0;
unsigned int Pulse_HP_buffer4[256] = {0};
unsigned char HB_counter4 = 0;

//一阶五点差分器
int Pulse5data[5] = {0};
int Pulse5data_out = 0;

//用于8点平滑滤波，得到类正弦波
unsigned int Pulse_HP_temp[16] = {0};
unsigned int Pulse_HP_Total_avg = 0;
unsigned int Pulse_HP_Total = 0;
unsigned int fil_counter = 0;


//峰峰值计算及SpO2计算
unsigned int Pulse_buffer_counter = 0;
unsigned int ind_min = 0;
unsigned int ind_max = 0;
unsigned int interval_counter = 0;
unsigned int Pulse_rate = 0;
float IR_buffer[Pulse_buffer_size];
float RD_buffer[Pulse_buffer_size];
int HP_buffer[Pulse_buffer_size];
float Pulse_amp = 0;
int Pulse = 0;
unsigned char peakEnable = 1;
unsigned char vallyEnable = 0;
unsigned int SPO2 = 0;
unsigned int heartRate = 0;
float Amp_threshold = 0.0002; //0.0002;
//float Amp_threshold = 0.0001;  //0910 展会

//计数器及标志位
unsigned char DAQ_enable = 0;
unsigned char LP_Done = 0;
unsigned int invalid_finger = 0;
//模式选择
int Pulse_HP_MAX = -0x00200000;
int Pulse_HP_MIN = 0x00200000;
int Pulse_HP_MAXReg = -0x00200000;
int Pulse_HP_MINReg = 0x00200000;
unsigned int SPO2_pre = 0;
float f_RD = 0;
float f_IR = 0;
float f_HP = 0;
float f_HP_min = 0;
unsigned char *p_RD;
unsigned char *p_IR;
unsigned int WaveCnt = 0;
float maxIRValue = 0;
float minIRValue = 1.5;
float maxRedValue = 0;
float minRedValue = 1.5;
float maxIRValueReg = 0;
float minIRValueReg = 1.5;
float maxRedValueReg = 0;
float minRedValueReg = 1.5;
unsigned int pulseStarted = 0; //0,1,2
int previousValue = Const3;
int currentValue = Const3;
int ppreviousValue = Const3;
int minPPGValue = Const3;
int maxPPGValue = Const3;
unsigned char minPPGValueGapCnt = 0;
unsigned int minIdTemp[50] = {0};
unsigned int maxIdTemp[50] = {0};
unsigned char cntMinAvg = 0;
unsigned char cntMaxAvg = 0;
unsigned int minIdSum = 0;
unsigned int maxIdSum = 0;

int vally[4] = {-1, -1, -1, -1}; //用于波谷检测
int peak[4] = {-1, -1, -1, -1}; //用于波峰检测
unsigned int pulseLength[3] = {0};

unsigned int avg_cnt = 0;
int filtIRSample = 0;
int filtIRSampleReg = 0;

unsigned int Pulse_weak = 0;
unsigned int motion_flag = 0;
unsigned int preSpO2 = 99;
unsigned int prePulseRate = 0;
unsigned int motion_cnt = 0;
unsigned int FSM_STATE = FSM_IDLE;

unsigned int SwitchCnt = 0;
unsigned char SPO2orBP = 0;
unsigned char SPO2orBP_Bef = 0;

unsigned char I2C2DATA_test = 0;
unsigned char flag_i2c = 0;
unsigned char I2Cdate_array[20];
unsigned char I2Ccnt = 0;
unsigned int int_cnt = 0;
unsigned int int_cnt1 = 0;
unsigned int int_cnt2 = 0;
unsigned int int_cnt3 = 0;
unsigned int int_cnt4 = 0;
//发送和显示的全局变量
unsigned char BPH_Num[10];
unsigned char BPL_Num[10];
unsigned char mmHg_Num[10];
unsigned char Pulse_N[10];
unsigned char Pulse_Num[10];
unsigned char SpO2_Num[10];
unsigned int Pulse_rate_OLED = 0;
unsigned int SPO2_OLED = 0;
unsigned char BefSleepFlag = 0;
unsigned char SPO2orBP_mmHg_cnt = 0;

unsigned int abc_test = 0;
unsigned int abc_test_dc = 0;


//去除AC和DC不合理的值
float irACtmp;
float redACtmp;
float irDCtemp;
float redDCtemp;

unsigned char ACMtemp;

//剧烈运动时，屏幕显示标志位
unsigned char motion_display_flag;

//打印SPO2值计数器
//char OLED_cnt;
//char OLED_cnt1;

//unsigned short Bloodpressure_cnt;   //显示实时压力时候，先对屏幕进行清零

/*配置位配置程序*/
// DEVCFG3
// USERID = No Setting
#pragma config PMDL1WAY = OFF           // Peripheral Module Disable Configuration (Allow multiple reconfigurations)
#pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#pragma config FVBUSONIO = OFF          // USB VBUS ON Selection (Controlled by Port Function)

// DEVCFG2
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
#pragma config FPLLMUL = MUL_16         // PLL Multiplier (16x Multiplier)
#pragma config FPLLODIV = DIV_2         // System PLL Output Clock Divider (PLL Divide by 2)

// DEVCFG1
#pragma config FNOSC = FRCDIV           // Oscillator Selection Bits (Fast RC Osc w/Div-by-N (FRCDIV))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = OFF            // Primary Oscillator Configuration (Primary osc disabled)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_1           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/1)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1              // Watchdog Timer Postscaler (1:1)
#pragma config WINDIS = OFF             // Watchdog Timer Window Enable (Watchdog Timer is in Non-Window Mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))
///#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window Size is 25%)

// DEVCFG0
#pragma config JTAGEN = OFF             // JTAG Enable (JTAG Disabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

/*
运动剔除状态机，当检测到超过阈值的运动时，则不进行血氧计算，维持上一次值
 */
void SportFSM() {
    switch (FSM_STATE) {
        case FSM_IDLE:
            if (motion_flag) {
                //putstringUART1("motion\n\r");
                FSM_STATE = FSM_MOTION1;
                motion_cnt++;
                motion_flag = 0;
            } else {
                //putstringUART1("IDLE\n\r");
                motion_cnt = 0;
                FSM_STATE = FSM_IDLE;
            }
            break;
        case FSM_MOTION1:
            //putstringUART1("AAA\n\r");
            //if (motion_cnt <= 60) //大概3s时间
            if (motion_cnt <= 50) //大概3s时间
            {
                motion_flag = 0;
                motion_cnt++; //motion_cnt==0时，才会对血氧进行计算，否则为原来值
                FSM_STATE = FSM_MOTION1;
            } else {
                if (motion_flag) //3s后，如果仍然检测到运动标志位，该位在ACM中设置
                {
                    FSM_STATE = FSM_MOTION2;
                    motion_cnt = 0;
                    motion_cnt++;
                    motion_flag = 0;
                    //Clear_SpO2_Num(); //把屏幕中的数据清空
                    //Clear_BPM_Num();
                    //LED_Display_Default();
                    if (SPO2orBP == 0) {
                        //putstringUART1("BBB\n\r");
                        //motion_display_flag = 1; //让屏幕显示默认值
                        LED_Display(2, Default_SpO2, sizeof (Default_SpO2) / 128); //显示默认值
                    }

                } else //若3s后，没有检测到运动标志位
                {
                    //putstringUART1("NO_motion1\n\r");
                    FSM_STATE = FSM_IDLE; //进入正常模式
                    vally[0] = -1; //重新进行血氧及脉率计算
                    vally[1] = -1;
                    vally[2] = -1;
                    vally[3] = -1;

                    motion_cnt = 0;

                    maxIRValue = 0;
                    maxRedValue = 0;
                    minIRValue = 1.5;
                    minRedValue = 1.5;
                    Pulse_HP_MAX = -0x00200000;
                    Pulse_HP_MIN = 0x00200000;
                    motion_display_flag = 0; //让屏幕显示默认值恢复正常显示
                }
            }
            break;
        case FSM_MOTION2: //又过了3s
            //if (motion_cnt <= 60) //在3s内，保持不计算
            if (motion_cnt <= 50) //在3s内，保持不计算
            {
                motion_flag = 0;
                motion_cnt++;
                FSM_STATE = FSM_MOTION2;
                avg_cnt = 0;
                vally[0] = -1;
                vally[1] = -1;
                vally[2] = -1;
                vally[3] = -1;
                pulseStarted = 0; //只有在寻找到一个波峰波谷后才 置1
                Pulse_HP_MAX = -0x00200000;
                Pulse_HP_MIN = 0x00200000;
                maxIRValue = 0;
                minIRValue = 1.5;
                maxRedValue = 0;
                minRedValue = 1.5;
                peakEnable = 1;
                vallyEnable = 0;

            } else {
                if (motion_flag) //3s后，如果仍为运动，那么进行休眠显示默认界面
                {
                    //putstringUART1("motion2_flag\n\r");
                    invalid_finger = 0;
                    interval_counter = 0;
                    avg_cnt = 0;
                    vally[0] = -1;
                    vally[1] = -1;
                    vally[2] = -1;
                    vally[3] = -1;
                    pulseStarted = 0;
                    maxIRValue = 0;
                    minIRValue = 1.5;
                    maxRedValue = 0;
                    minRedValue = 1.5;
                    Pulse_HP_MAX = -0x00200000;
                    Pulse_HP_MIN = 0x00200000;
                    peakEnable = 1;
                    vallyEnable = 0;
                    Pulse_weak = 0;
                    motion_flag = 0;
                    /*motion_cnt = 0;
                    FSM_STATE = FSM_IDLE;*/
                    motion_cnt = 61;
                    FSM_STATE = FSM_MOTION2; //依然判断是否处于运动状态
                    if (ReadI2C2Flag == 0 && SPO2orBP == 0) {
                        LED_Display(2, Default_SpO2, sizeof (Default_SpO2) / 128); //显示默认值
                        //motion_display_flag = 1; //让屏幕显示默认值
                        //GotoSleep(); //进入休眠		
                        //putstringUART1("CCC\n\r");
                    }
                    //同时关闭AFE中断，等不晃了重新打开AFE中断
                    IEC0bits.INT0IE = 0;
                    IFS0bits.INT0IF = 0;
                    /*putstringUART1("SPO2\n\r");
                    puthexUART1(SPO2_OLED_display);
                    putstringUART1("\n\r");
                    putstringUART1("PLUSE\n\r");
                    puthexUART1(Pulse_rate_OLED_display);
                    putstringUART1("\n\r");*/
                } else //否则，重新开始计算血氧
                {
                    //putstringUART1("NO_motion2\n\r");
                    FSM_STATE = FSM_IDLE;
                    vally[0] = -1;
                    vally[1] = -1;
                    vally[2] = -1;
                    vally[3] = -1;
                    motion_cnt = 0;
                    motion_flag = 0;
                    Pulse_HP_MAX = -0x00200000;
                    Pulse_HP_MIN = 0x00200000;

                    maxIRValue = 0;
                    //minIRValue = 1.5;
                    maxRedValue = 0;
                    minIRValue = 1.5;
                    minRedValue = 1.5;
                    //motion_display_flag = 0; //让屏幕显示默认值恢复正常显示
                    //不晃动，AFE重新开始检测
                    IEC0bits.INT0IE = 1;
                    IFS0bits.INT0IF = 0;
                }
            }
            break;
    }
}

/****************************************
函数名：Timer4Handler(void)描述：T4中断服务程序
主要用于定时显示波形和数据刷新
 **************************************/
//void __ISR(_TIMER_4_VECTOR, ipl2) Timer4Handler(void) //中断向量 对应 中断优先级，见总手册表7.1

void __ISR(_TIMER_4_VECTOR, ipl1) Timer4Handler(void) //中断向量 对应 中断优先级，见总手册表7.1
{
    IFS0bits.T4IF = 0; //清T4中断flag	
    LED_Display_Timer4En = 1;
    //动态调整波形刷新速度
    //PR4=(Pulse_rate<<8)+100;
    /*//if (BefSleepFlag == 0 && LED_Display_Timer4En == 1) {
    if (BefSleepFlag == 0) {
        //LED_Display_Timer4En = 0;
        LED_Display_Change();
    }*/
}

/*
AFE数据采样中断处理程序
 */
void __ISR(_EXTERNAL_0_VECTOR, ipl5) INT0Handler(void) {
    IEC0bits.INT0IE = 0;
    IFS0bits.INT0IF = 0;

    decIR_signal = (AFE44x0_Reg_Read(0x2E) + 0x00200000) & 0x003fffff; //这些数据已经是减去背景的数据，AFE已做处理
    decRD_signal = (AFE44x0_Reg_Read(0x2F) + 0x00200000) & 0x003fffff;

    decIR_raw = (AFE44x0_Reg_Read(0x2A) + 0x00200000) & 0x003fffff; //未去除背景光数据
    decRD_raw = (AFE44x0_Reg_Read(0x2C) + 0x00200000) & 0x003fffff;

    decIR_A = (AFE44x0_Reg_Read(0x2B) + 0x00200000) & 0x003fffff; //未去除背景光数据
    decRD_A = (AFE44x0_Reg_Read(0x2D) + 0x00200000) & 0x003fffff;
    IFS0bits.INT0IF = 0;
    IEC0bits.INT0IE = 1;
    filter4(); //把采样到的数据进行4点平滑滤波，可有效去除工频干扰
    if (LP_Done == 1) //该值在filter4函数中置1
    {
        LP_Done = 0;
        ModelSelect(); //根据采样到的光强数据，判断是否有手指，如果有，则正常模式，如果没有，则等待模式
        send_uart(); //把数据通过UART发送			
        while (!U1STAbits.TRMT); //确保上一次已经发送完成       
    }
}

/*
加速度传感器中断处理程序
 */
//void __ISR(_EXTERNAL_4_VECTOR, ipl7) INT4Handler(void) {

void __ISR(_EXTERNAL_4_VECTOR, ipl2) INT4Handler(void) {
    IEC0bits.INT4IE = 0;
    IFS0bits.INT4IF = 0;
    ACM_INT_Process();
    //IFS0bits.INT4IF = 0;   
    IEC0bits.INT4IE = 1;
}

/*
按钮外部中断
当按钮按下时，一方面关闭对应按钮中断，同时启动T3进行计时，500ms后，关闭T3，打开
外部按钮中断。
 */
void __ISR(_EXTERNAL_1_VECTOR, ipl6) INT1Handler(void) //外部switch中断
{

    //char temp1[20];
    //unsigned int ArrLen_tmp = 0;
    unsigned short i;
    IEC0bits.INT1IE = 0;
    IFS0bits.INT1IF = 0;

    TMR3 = 0;
    PR3 = 0xF9F; //1ms,一次按钮之后，500ms内不再产生中断
    T3CONSET = 0x8000; // 开始计数
    if (SleepFlag); //若此时Sleep标志位为1，说明是从休眠启动，所以只是起唤醒作用
    else //否则进行显示的切换，血氧/血压
    {
        if (SPO2orBP == 0) { //血氧
            SPO2orBP_Bef = SPO2orBP;
            //test      
            //HimmHg += 10;
            //LowmmHg += 10;
            //PR_BP += 10;
            //            
            SPO2orBP = 1;
        } else if (SPO2orBP == 1) {//血压
            SPO2orBP_Bef = SPO2orBP;
            SPO2orBP = 0;
        }
        /*
                    SPO2orBP = 2;//test
                    SPO2orBP_mmHg_cnt = 0;
                    mmHg+=10;
                    } else if (SPO2orBP = 2) {//切换到2是调试用
                        SPO2orBP_Bef = SPO2orBP;
                      SPO2orBP = 0;
                }
         */
    }
    /*
{
    LED_Display_ClearMain();
    if (SPO2orBP == 0) //血压，此处测血压时自动切换到血压显示界面，实时显示压力值，最大300mmHg,最后显示高低压和脉率
    {
        LED_Display_Default_BP(); //显示血压斜杠
        ArrLen_tmp = sizeof (BPx) / 128;
        LED_Display(0，BPx, ArrLen_tmp);
        u32tostr(HimmHg, BPH_Num, temp1);
        LED_Display_D荬内ynamic_BP(0x18, 0x02, BPH_Num);
        u32tostr(LowmmHg, temp1, BPL_Num);
        LED_Display_Dynamic_BP_Inv(0x2C, 0x02, BPL_Num);
        u32tostr(PR_BP, Pulse_N, temp1);
        LED_Display_Dynamic_BP(0x6C, 0x02, Pulse_N);
        SPO2orBP = 1;
    } else if (SPO2orBP) //血氧
    {
        ArrLen_tmp = sizeof (SpO2x) / 128;
        LED_Display(0，SpO2x, ArrLen_tmp);
        if (Pulse_rate_LP == 0)
            Pulse_rate_OLED = 0;
        else
            Pulse_rate_OLED = Pulse_rate_LP & 0x000000ff;
        u32tostr(Pulse_rate_OLED, Pulse_Num, temp1);
        SPO2_OLED = SpO2_LP & 0x000000ff;
        u32tostr(SPO2_OLED, SpO2_Num, temp1);
        LED_Display_Dynamic(0x60, 0x02, Pulse_Num);
        LED_Display_Dynamic(0x28, 0x02, SpO2_Num);
        SPO2orBP = 0;
    }
    //SW_Interrupt();
}*/
    IFS0bits.INT1IF = 0;
    IEC0bits.INT1IE = 0; //每次中断后，不使能中断，需要定时器使能
    IFS0bits.T3IF = 0;
    IEC0bits.T3IE = 1;
}
//T3的优先级更低一点，进一次INT3后，开启计数，同时INT3不使能，
//计数到500ms之后，关闭T3，同时使能INT3。

void __ISR(_TIMER_3_VECTOR, ipl3) Timer3Handler(void) {
    IEC0bits.T3IE = 0;
    IFS0bits.T3IF = 0;

    IEC0bits.INT1IE = 0; //INT3不允许中断
    IFS0bits.INT1IF = 0;
    SwitchCnt++;
    if (SwitchCnt >= 500) //超过500ms
    {
        SwitchCnt = 0;
        T3CONbits.ON = 0;
        IFS0bits.INT1IF = 0;
        IEC0bits.INT1IE = 1; //INT3允许中断
        IFS0bits.T3IF = 0;
        IEC0bits.T3IE = 0;
    } else {
        IFS0bits.T3IF = 0;
        IEC0bits.T3IE = 1;
    }
}

/*
I2C2中断服务程序
用于与血压通讯。Comm_Process内部主要是一个状态机，用于数据发送与接收
 */
void __ISR(_I2C_2_VECTOR, ipl4) I2C2Handler(void) {
    IFS1bits.I2C2SIF = 0;
    Comm_Process();
    int_cnt++;
}

/****************************************
函数名：void main()
描述：主函数，包括初始化程序，滤波程序以及峰峰值查找和血氧值计算程序
 *****************************************/

void main() {
    BefSleepFlag = 0;
    ReadI2C2Flag = 0;
    SPO2orBP = 0;
    SPO2orBP_Bef = SPO2orBP;
    WDTCONbits.ON = 0;
    SleepFlag = 0;
    Pulse_weak = 0;
    motion_flag = 0;
    initPORTs(); //对IO管脚进行配置
    initUART1(8); //配置波特率  115200
    ///initRTCC(InitDate, InitTime);//不用该功能

    initSPI2_Master();
    initSPI1_Master();
    IEC1bits.SPI1RXIE = 0;
    IFS1bits.SPI1RXIF = 0;
    AFE_init();
    AFE_Interrupt_init();
    initI2C1();
    initACM();
    ACMint_init();

    INTCONbits.MVEC = 1; //采用多向量模式
    //INTEnableInterrupts(); //使能全局中断，CPU寄存器中由status寄存器控制 

    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);

    // enable interrupts
    INTEnableInterrupts();

    Init_OLED();
    initTimer4();
    initTimer3();
    SW_Interrupt_init();
    //initI2C2();

    while (1) {
        if (BefSleepFlag == 0 && LED_Display_Timer4En == 1) {
            LED_Display_Timer4En = 0;
            LED_Display_Change();
        }
        GotoIdle(); //每次采样周期结束后，进入IDLE模式	
    }
}

/****************************************
函数名：filter4()
描述：4点平滑滤波程序，将BG/IR/RD转换为光强数据，并做滤波，同时还包括脉率信号和血氧信号
 **************************************/
void filter4() {
    unsigned char i, j;
    //desampling
    RD_Intensity = RD_Intensity + decRD_signal;
    IR_Intensity = IR_Intensity + decIR_signal;
    Desampling = Desampling + 1;

    if (Desampling >= Desampling_rate) //四次采集后才进入
    {
        Desampling = 0;
        IO_RA2 = ~IO_RA2;
        //LP pass
        RD = (RD + (RD_Intensity >> 2)) >> 1; //光滑滤波  

        IR = (IR + (IR_Intensity >> 2)) >> 1; //光滑滤波    
        //        SendDecimalismDta(IR);
        //        putstringUART1("\r\n");
        RD_Intensity = 0;
        IR_Intensity = 0;

        //                SendDecimalismDta(IR_HP);
        //                putstringUART1("\r\n");

        //低通
        if (IR_RD_HLP)
            for (i = 0; i < LP_IR_RD_f_bit; i++)
                IR_LP_pass[i] = IR;
        //Pulse_LP_temp = Pulse_HP;
        IR_LP_pass[0] = IR;
        for (i = 0; i < LP_IR_RD_f_orders; i++) {
            for (j = 0; j < LP_IR_RD_f; j++) {
                IR_LP_temp += IR_LP_pass[j];
                //                    SendDecimalismDta(Pulse_LP_temp);
                //        putstringUART1("\r\n");
                IR_LP_pass[j + 1] = IR_LP_pass[j];
            }
            IR_LP_out[i] = IR_LP_temp>>LP_IR_RD_f_bit;

            IR_LP_pass[0] = IR_LP_out[i];
            IR_LP_temp = 0;
        }
        IR_LP = IR_LP_out[LP_IR_RD_f_orders - 1];
        //                                SendDecimalismDta(IR_BP);
        //                                putstringUART1("\r\n");

        //---------------------------------------------- 

        //            SendDecimalismDta(Pulse_HP);
        //            putstringUART1("\r\n");  
        //低通
        if (IR_RD_HLP)
            for (i = 0; i < LP_f_bit; i++)
                RD_LP_pass[i] = RD;
        //Pulse_LP_temp = Pulse_HP;
        RD_LP_pass[0] = RD;
        for (i = 0; i < LP_f_orders; i++) {
            for (j = 0; j < LP_f; j++) {
                RD_LP_temp += RD_LP_pass[j];
                //                    SendDecimalismDta(Pulse_LP_temp);
                //        putstringUART1("\r\n");
                RD_LP_pass[j + 1] = RD_LP_pass[j];
            }
            RD_LP_out[i] = RD_LP_temp>>LP_f_bit;

            RD_LP_pass[0] = RD_LP_out[i];
            RD_LP_temp = 0;
        }
        RD_LP = RD_LP_out[LP_f_orders - 1];
        //                                                SendDecimalismDta(RD_BP);
        //                                                putstringUART1("\r\n");

        IR_RD_HLP = 0;

        f_RD = ((float) (RD_LP)) / 4194303 * 2.4 - 1.2; //转换为对应电压
        f_IR = ((float) (IR_LP)) / 4194303 * 2.4 - 1.2;

        //        SendDecimalismDta(f_IR * 10000);
        //        putstringUART1("\r\n");
        LP_Done = 1;




    }
}

/****************************************
函数名：ModelSelect()
描述：将光强信号（主要是RD）进行比较，若不在合理范围内，则视为没有手指，则切换模式。
      在等待模式下超过10s，系统进行SLEEP模式。同时在切换过程中对BASE进行控制，降低功耗
 **************************************/
void ModelSelect() {
    if ((f_IR >= Hz_upper_threshold) || (f_IR <= Hz_lower_threshold)) {
        Bef_GotoSleep(); //若光强在设定范围外，就认为异常，一定时间后就休眠	
    } else {
        if (BefSleepFlag == 1)
            LED_Fill(0);
        IFS0bits.T4IF = 0; //重新开启屏幕刷新
        IEC0bits.T4IE = 1; // 使能T4中断
        BefSleepFlag = 0; //否则就进行正常的数据处理
        invalid_finger = 0;
        Pulse_rate_fn(); //数据处理函数
    }
}

/*
休眠前的处理函数
 */
void Bef_GotoSleep() {
    invalid_finger = invalid_finger + 1;
    if (invalid_finger < 250) //一定时间以内，清零之前计算的数据
    {
        vally[0] = -1;
        vally[1] = -1;
        vally[2] = -1;
        vally[3] = -1;
        pulseStarted = 0;
        maxIRValue = 0;
        minIRValue = 1.5;
        maxRedValue = 0;
        minRedValue = 1.5;
        Pulse_HP_MAX = -0x00200000;
        Pulse_HP_MIN = 0x00200000;
        peakEnable = 1;
        vallyEnable = 0;
        Pulse_weak = 0;
    }
    if (invalid_finger >= no_finger_threshold) //超过时间阈值
    { //且未在发数状态机内
        if (ReadI2C2Flag == 0 && SPO2orBP != 2)//在测血压时不休眠
            GotoSleep(); //进入休眠	
    } else if (invalid_finger >= (no_finger_threshold - 250)) {
        if (invalid_finger <= (no_finger_threshold - 249)) {
            //putstringUART1("NO figure!\r\n");
            IFS0bits.T4IF = 0;
            IEC0bits.T4IE = 0; // 关闭T4中断，不进行屏幕刷新
            LED_Fill(0x00); // 清屏
        }
        BefSleepFlag = 1;
        LED_Display(2, No_Finger, sizeof (No_Finger) / 128);
        interval_counter = 0;
        avg_cnt = 0;
    }
}

/*
滤波函数
首先将输入的波形，进行高通滤波，去掉基线
之后，进行8个数据的平滑滤波，达到低通的效果，频率大概在3Hz左右
 */
unsigned int FilterHP(int sample) {
    //        SendDecimalismDta(sample);
    //        putstringUART1("\r\n");
    HB_counter++;
    if (HB_counter > HP_f) //高通滤波，去基线        
        HB_counter = 0;
    Pulse_HP = sample + Const3; //const3防止有负数的情况        
    Pulse_HP = Pulse_HP - (Pulse_HP_templ >> 8); //÷32是数组里面32个数做个平均。
    Pulse_HP_templ = Pulse_HP_templ - Pulse_HP_buffer[HB_counter] + sample; //这个的思路很巧妙  (但会有溢出的可能)
    Pulse_HP_buffer[HB_counter] = sample; //32个数里面减去最老的那个数，加上最新的数。
    //    截止频率需设置在4.16Hz以外，否则截止点心率不正确，大于截止点、小于250的通过旁瓣通过识别到了
    //    截止频率需设置在3.125Hz以外，则又会导致65bpm心率不正确
    //        abc_test = Pulse_HP; //用于数据打印

    fil_counter++; //用上述方法实现8个数的平滑滤波（6.25Hz），得到Pulse_HP_Total_avg
    if (fil_counter > 7) //该信号用于峰峰值判断
        fil_counter = 0;
    Pulse_HP_Total_avg = Pulse_HP_Total >> 3; //>> 4; //均值滤波（低通滤波）
    Pulse_HP_Total = Pulse_HP_Total - Pulse_HP_temp[fil_counter] + Pulse_HP;
    Pulse_HP_temp[fil_counter] = Pulse_HP;
    Pulse = Pulse_HP_Total_avg;
    return Pulse;
}


//一阶五点差分器、高通滤除基线、低通滤除肌电干扰

int FilterHP1(int sample) {
    //    SendDecimalismDta(sample);
    //    putstringUART1("\r\n");
    unsigned char i, j;
    //    一阶五点差分器:DATA(i)=(2*DATA(i)+1*DATA(i-1)-1*DATA(i-3)-2*DATA(i-4))/10;
    if (Pulse_HLP)
        for (i = 0; i < 5; i++)
            Pulse5data[i] = sample;
    Pulse5data[0] = sample;
    Pulse5data[0] = ((Pulse5data[0] << 1) + Pulse5data[1] - Pulse5data[3] - (Pulse5data[4] << 1)) / 10;
    for (i = 0; i < 4; i++)
        Pulse5data[4 - i] = Pulse5data[3 - i];
    //Pulse5data_out = Pulse5data[0];
    //    SendDecimalismDta(Pulse5data_out);
    //    putstringUART1("\r\n");
    //    Pulse_HLP = 0;
    //    return Pulse5data_out;

    //高通滤除基线
    if (Pulse_HLP)
        for (i = 0; i < HP_f_orders; i++)
            Pulse_HP_pass[i] = Pulse5data[0];
    Pulse_HP_templ = Pulse5data[0];

    for (i = 0; i < HP_f_orders; i++) {
        Pulse_HP_out[i] = (Pulse_HP_templ - Pulse_HP_pass[i]) + Const3; //有负数出现   
        Pulse_HP_pass[i] = Pulse_HP_pass[i] - (Pulse_HP_pass[i] >> HP_f_bit) + (Pulse_HP_templ >> HP_f_bit);
        Pulse_HP_templ = Pulse_HP_out[i];
    }
    Pulse_HP_pass[0] = Pulse_HP_pass[HP_f_orders - 1];
    Pulse_HP = Pulse_HP_out[HP_f_orders - 1];

    //            SendDecimalismDta(Pulse_HP);
    //            putstringUART1("\r\n");  
    //低通
    if (Pulse_HLP)
        for (i = 0; i < LP_f_bit; i++)
            Pulse_LP_pass[i] = Pulse_HP;
    //Pulse_LP_temp = Pulse_HP;
    Pulse_LP_pass[0] = Pulse_HP;
    for (i = 0; i < LP_f_orders; i++) {
        for (j = 0; j < LP_f; j++) {
            Pulse_LP_temp += Pulse_LP_pass[j];
            //                    SendDecimalismDta(Pulse_LP_temp);
            //        putstringUART1("\r\n");
            Pulse_LP_pass[j + 1] = Pulse_LP_pass[j];
        }
        Pulse_LP_out[i] = Pulse_LP_temp>>LP_f_bit;

        Pulse_LP_pass[0] = Pulse_LP_out[i];
        Pulse_LP_temp = 0;
    }
    Pulse = Pulse_LP_out[LP_f_orders - 1];
    Pulse_HLP = 0;
    //            SendDecimalismDta(Pulse);
    //            putstringUART1("\r\n");

    return Pulse;

}

/*
血氧计算函数
参数：红外最大值、红外最小值、红光最大值、红光最小值
 */
unsigned int calcPulseOx(float maxIR, float minIR, float maxRed, float minRed) {
    float irDC = minIR;
    float irAC = maxIR - minIR;
    float redDC = minRed;
    float redAC = maxRed - minRed;
    unsigned int tSpO2 = 0;
    //abc_test = maxRed;   //用于数据打印
    //abc_test_dc = minRed; //用于数据打印 



    float ratio = (redAC / redDC) / (irAC / irDC);
    //    putstringUART1("\r\n");
    //    SendDecimalismDta(ratio*10000);
    //    tSpO2 = (120.7 - 40.04 * ratio) + 0.5; //7.29 N-20,20p,RED+IR,SENSOR:#059
    tSpO2 = (120.6 - 38.17 * ratio) + 0.5; //直线20160307 Nellcor
    //tSpO2 =-21.28*ratio*ratio-0.2596*ratio+105;//二次多项式20160307 Nellcor
    //    SendDecimalismDta(tSpO2);
    //    putstringUART1("\r\n");
    return tSpO2;
}

/*
//脉率计算函数
参数：两次峰峰值的计数值
 */

unsigned int calcHeartRate(unsigned int len0, unsigned len1, unsigned len2) {
    float length = 0;
    length = (len0 + len1 + len2) / 3.0;
    //Calculate length of pulse based on start and end indices
    //    putstringUART1("\r\npulseLength：\r\n");
    //        SendDecimalismDta(length * 100);
    //        putstringUART1("\r\n");
    //Check if this is reasonable pulse length
    //if ((length >= sampleRate / 4) && (length < sampleRate * 3)) {//20-250脉率
    if ((length >= 12) && (length <= 100)) {//30-250脉率
        heartRate = (60.00 * sampleRate) / length + 0.5;
        //putstringUART1("\r\nPulse_rate：\r\n");
        //puthexUART1(heartRate);
        //SendDecimalismDta(heartRate);
        //return heartRate;
    }
    //        putstringUART1("\r\nPulse_rate：0\r\n");
    return heartRate; //Return 0 for invalid pulse length
}

/****************************************
函数名：Pulse_rate_fn
描述：脉率计算和SpO2计算函数，同时包括高通滤波和峰峰值查询。若峰峰值查询超时则视为低灌注
      如果超过时间过长，则视为不是手指，系统进入SLEEP模式
 **************************************/
//kebin 20160219

unsigned int Pulse_rate_fn() {
    interval_counter++;
    filtIRSample = FilterHP1(IR_LP + RD_LP);
    //    putstringUART1("interval_counter=\r\n");
    //    SendDecimalismDta(interval_counter);
    //    putstringUART1("\r\n");
    //    putstringUART1("filtIRSample=\r\n");
    //                SendDecimalismDta(filtIRSample);
    //                putstringUART1("\r\n"); 
    ppreviousValue = previousValue; //3个数据，用于峰值的计算
    previousValue = currentValue;
    currentValue = filtIRSample;
    filtIRSampleReg = (-1) * filtIRSample;    
    //        用于波形归一化显示。最大最小值都是实时找出来的，在不断更新，只是
    //        是上一个周期的
    if (Pulse_HP_MINReg != 0x00200000) {
        filtIRSampleReg = filtIRSampleReg < Pulse_HP_MINReg ? Pulse_HP_MINReg : filtIRSampleReg;
        filtIRSampleReg = filtIRSampleReg > Pulse_HP_MAXReg ? Pulse_HP_MAXReg : filtIRSampleReg;
        f_HP = (float) (filtIRSampleReg - Pulse_HP_MINReg) / (float) (Pulse_HP_MAXReg - Pulse_HP_MINReg);
        //            putstringUART1("f=\r\n");
        //            SendDecimalismDta(f_HP * 10000);
        //            putstringUART1("\r\n");
    } else
        f_HP = 0;

    //用于数据未稳定时的波形显示，波形翻转
    if (pulseStarted == 0) {
        if ((previousValue <= ppreviousValue) && (previousValue < currentValue)&& (previousValue < Const3))
            Pulse_HP_MAXReg = -1 * previousValue;
        if ((previousValue >= ppreviousValue) && (previousValue > currentValue)&&(previousValue > Const3))
            Pulse_HP_MINReg = -1 * previousValue;
    }

    //滤除刚开始不稳定数据后获取最大值和最小值点，相对稳定些再计算400*0.02=8
    if ((interval_counter > Pulse_Index_Start)&&(interval_counter <= (Pulse_Index_Start + 200))) {
        //putstringUART1("1700>interval_counter > 1500\r\n");
        //        putstringUART1("previousValue=\r\n");
        //        SendDecimalismDta(previousValue);
        //        putstringUART1("\r\n"); 
        if ((previousValue <= ppreviousValue) && (previousValue < currentValue)&& (previousValue < Const3))
            if (minPPGValue > previousValue)
                minPPGValue = previousValue;
        if ((previousValue >= ppreviousValue) && (previousValue > currentValue)&&(previousValue > Const3))
            if (maxPPGValue < previousValue)
                maxPPGValue = previousValue;
//        putstringUART1("minPPGValue=\r\n");
//        SendDecimalismDta(minPPGValue);
//        putstringUART1("\r\n");
//        putstringUART1("maxPPGValue=\r\n");
//        SendDecimalismDta(maxPPGValue);
//        putstringUART1("\r\n");
    }

    if (interval_counter > (Pulse_Index_Start + 200)) {
        unsigned short i;
        unsigned int index = interval_counter - (Pulse_Index_Start + 200);
        //判断波谷值是否发生了突变
        if ((previousValue <= ppreviousValue) && (previousValue < currentValue)&& (previousValue < Const3)) {
            //            putstringUART1("a=\r\n");
            //            SendDecimalismDta(previousValue);
            //            putstringUART1("\r\n");
            //            putstringUART1("b=\r\n");
            //            SendDecimalismDta((Min_PPG_Gap_threshold * minPPGValue));
            //            putstringUART1("\r\n");
            if (previousValue < (Min_PPG_Gap_threshold * minPPGValue))
                if (fabs(minPPGValue - previousValue) > Min_PPG_Gap) {//发生突变
                    //                    putstringUART1("c=\r\n");
                    //                    SendDecimalismDta(fabs(previousValue - minPPGValue));
                    //                    putstringUART1("\r\n");
                    minPPGValueGapCnt++;
//                    putstringUART1("Cnt=\r\n");
//                    SendDecimalismDta(minPPGValueGapCnt++);
//                    putstringUART1("\r\n");
                    if (minPPGValueGapCnt > MinPPGValueGapCntMax) {
//                        putstringUART1("CntIn.\r\n");
                        minPPGValueGapCnt = 0;
                        minPPGValue = Const3;
                        maxPPGValue = Const3;
                        interval_counter = 0;
                        vally[0] = -1;
                        vally[1] = -1;
                        vally[2] = -1;
                        vally[3] = -1;
                        pulseStarted = 0;
                        Pulse_HP_MAX = -0x00200000;
                        Pulse_HP_MIN = 0x00200000;
                        maxIRValue = 0;
                        minIRValue = 1.5;
                        maxRedValue = 0;
                        minRedValue = 1.5;
                        peakEnable = 1;
                        vallyEnable = 0;
                        return;
                    }
                } else
                    minPPGValueGapCnt = 0;
        }


        //        Find max and min IR and Red values in the current pulse      
        IR_buffer[index - 1] = f_IR; //把对应的电压数据和
        RD_buffer[index - 1] = f_RD; //滤波后数据放入数组
        HP_buffer[index - 1] = filtIRSample; //用于后续的血氧和脉率计算 





        //用3Hz的波形进行峰值寻找，同时
        //用实际波形的最大最小值进行血氧计算
        //找峰值(为什么每次找到最小值就可以进行SPO2等值的计算了，因为类似于正弦波的波形，最大值比最小值先出现，如果最小值找到了，那这个周期的峰峰值寻找完毕)
        if ((previousValue <= ppreviousValue) && (previousValue < currentValue)&& (previousValue < Const3))//&& (!Set_enable)//&& (high_cnt >= 4))//&&(currentValue <(Const3-5000)))  //检测最小值
        {
            if (minPPGValue > previousValue)
                minPPGValue = previousValue;
            //                        putstringUART1("Min_Value_threshold * minValue=\r\n");
            //                        SendDecimalismDta(Min_Value_threshold * minValue);
            //                        putstringUART1("\r\n");
            //                        putstringUART1("previousValue=\r\n");
            //                        SendDecimalismDta(previousValue);
            //                        putstringUART1("\r\n");
            //if (previousValue < (Min_Value_threshold * minValue)) {
            if ((peakEnable == 1)&&(previousValue < (Min_Value_threshold * minPPGValue))) {
                minIdTemp[cntMinAvg] = index - 1;
                //Pulse_HP_MIN = previousValue;
                //                                putstringUART1("minIdTemp[cntMinAvg] =\r\n");
                //                                SendDecimalismDta(minIdTemp[cntMinAvg]);
                //                putstringUART1("\r\n");
                //                                SendDecimalismDta(cntMinAvg);
                //                                putstringUART1("\r\n");
                if (cntMinAvg != 0) {
                    if ((((minIdTemp[cntMinAvg] + 300 - minIdTemp[cntMinAvg - 1]) % 300) > 7)) { //已找到一下个波峰
                        //if (((minIdTemp[cntMinAvg] - minIdTemp[cntMinAvg - 1]) > 7)) {
                        //取前一个波峰的平均位置;
                        if (vally[0] == -1) {//第一个峰谷
                            vally[0] = minIdSum / cntMinAvg;
                            //                            putstringUART1("aa\r\n");
                        } else if (vally[1] == -1) //第二个峰谷
                        {
                            vally[1] = minIdSum / cntMinAvg;
                            //对应脉率检测标志位置位
                            //                            putstringUART1("cc\r\n");
                        } else if (vally[2] == -1)//第三个峰谷 
                        {
                            vally[2] = minIdSum / cntMinAvg;
                            //                            putstringUART1("dd\r\n");
                        } else if (vally[3] == -1) {//第四个峰谷
                            vally[3] = minIdSum / cntMinAvg;
                            //                            putstringUART1("ee\r\n");
                        } else {
                            vally[0] = vally[1];
                            vally[1] = vally[2];
                            vally[2] = vally[3];
                            vally[3] = minIdSum / cntMinAvg;
                            pulseStarted = 1;
                            //                            putstringUART1("ff\r\n");
                        }
                        //                        putstringUART1("vally[0]=\r\n");
                        //                        SendDecimalismDta(vally[0]);
                        //                        putstringUART1("\r\n");
                        //                        putstringUART1("vally[1]=\r\n");
                        //                        SendDecimalismDta(vally[1]);
                        //                        putstringUART1("\r\n");
                        //                        putstringUART1("vally[2]=\r\n");
                        //                        SendDecimalismDta(vally[2]);
                        //                        putstringUART1("\r\n");
                        //                                                putstringUART1("vally[3]=\r\n");
                        //                                                SendDecimalismDta(vally[3]);
                        //                                                putstringUART1("\r\n");
                        //                        putstringUART1("minIdSum=\r\n");
                        //                        SendDecimalismDta(minIdSum);
                        //                        putstringUART1("\r\n");
                        //                        putstringUART1("cntMinAvg=\r\n");
                        //                        SendDecimalismDta(cntMinAvg);
                        //                        putstringUART1("\r\n");
                        vallyEnable = 1; //互锁
                        peakEnable = 0;
                        minIdSum = 0;
                        minIdTemp[0] = minIdTemp[cntMinAvg]; //开始寻找新的波峰
                        cntMinAvg = 0;

                    }
                }
                minIdSum = minIdSum + minIdTemp[cntMinAvg];
                cntMinAvg = cntMinAvg + 1;
            }
        }
        //最大值检测
        if ((previousValue >= ppreviousValue) && (previousValue > currentValue)&&(previousValue > Const3)) {
            if (maxPPGValue < previousValue)
                maxPPGValue = previousValue;
            //                        putstringUART1("Max_Value_threshold * maxValue=\r\n");
            //                        SendDecimalismDta(Max_Value_threshold * maxValue);
            //                        putstringUART1("\r\n");
            //                        putstringUART1("previousValue=\r\n");
            //                        SendDecimalismDta(previousValue);
            //                        putstringUART1("\r\n");

            //if (previousValue > (Max_Value_threshold * maxValue)) {
            if ((vallyEnable == 1)&&(previousValue > (Max_Value_threshold * maxPPGValue))) {
                maxIdTemp[cntMaxAvg] = index - 1;
                //Pulse_HP_MAX = previousValue;
                //                putstringUART1("maxIdTemp[cntMaxAvg] =\r\n");
                //                SendDecimalismDta(maxIdTemp[cntMaxAvg]);
                //                putstringUART1("\r\n");
                //                SendDecimalismDta(cntMaxAvg);
                //                putstringUART1("\r\n");
                if (cntMaxAvg != 0) {
                    if ((((maxIdTemp[cntMaxAvg] + 300 - maxIdTemp[cntMaxAvg - 1]) % 300) > 7)) {
                        //if (((maxIdTemp[cntMaxAvg] - maxIdTemp[cntMaxAvg - 1]) > 7)) {
                        //取前一个波谷的平均位置                            
                        if (peak[0] == -1) {
                            peak[0] = maxIdSum / cntMaxAvg; //第一个波谷                                    
                            //putstringUART1("aaa\r\n");
                        } else if (peak[1] == -1) //第二个波谷
                        {
                            peak[1] = maxIdSum / cntMaxAvg;
                            //putstringUART1("bbb\r\n");
                        } else if (peak[2] == -1)//第三个波谷
                        {
                            peak[2] = maxIdSum / cntMaxAvg;
                            //putstringUART1("ccc\r\n");
                        } else if (peak[3] == -1) {//第四个波谷
                            peak[3] = maxIdSum / cntMaxAvg;
                            //putstringUART1("ddd\r\n");
                        } else {
                            peak[0] = peak[1];
                            peak[1] = peak[2];
                            peak[2] = peak[3];
                            peak[3] = maxIdSum / cntMaxAvg;

                            //putstringUART1("eee\r\n");
                        }
                        //                        putstringUART1("peak[0]=\r\n");
                        //                        SendDecimalismDta(peak[0]);
                        //                        putstringUART1("\r\n");
                        //                        putstringUART1("peak[1]=\r\n");
                        //                        SendDecimalismDta(peak[1]);
                        //                        putstringUART1("\r\n");
                        //                        putstringUART1("peak[2]=\r\n");
                        //                        SendDecimalismDta(vally[2]);
                        //                        putstringUART1("\r\n");
                        //                        putstringUART1("peak[3]=\r\n");
                        //                        SendDecimalismDta(peak[3]);
                        //                        putstringUART1("\r\n");
                        vallyEnable = 0; //互锁
                        peakEnable = 1;
                        maxIdSum = 0;
                        maxIdTemp[0] = maxIdTemp[cntMaxAvg]; //开始寻找新的波谷
                        cntMaxAvg = 0;

                    }
                }
                maxIdSum = maxIdSum + maxIdTemp[cntMaxAvg];
                cntMaxAvg = cntMaxAvg + 1;
            }
        }

        //------------------------------------------------------------------
        if (vally[0] == -1 || vally[1] == -1 || vally[2] == -1 || vally[3] == -1)
            pulseStarted = 0;
        else {
            if (vally[0] > vally[1]) {
                pulseLength[0] = vally[1] + 300 - vally[0];
            } else
                pulseLength[0] = vally[1] - vally[0];
            if (vally[1] > vally[2]) {
                pulseLength[1] = vally[2] + 300 - vally[1];
            } else
                pulseLength[1] = vally[2] - vally[1];
            if (vally[2] > vally[3]) {
                pulseLength[2] = vally[3] + 300 - vally[2];
            } else
                pulseLength[2] = vally[3] - vally[2];
            putstringUART1("0=\r\n");
            SendDecimalismDta(pulseLength[0]);
            putstringUART1("\r\n");
            putstringUART1("1=\r\n");
            SendDecimalismDta(pulseLength[1]);
            putstringUART1("\r\n");
            putstringUART1("2=\r\n");
            SendDecimalismDta(pulseLength[2]);
            putstringUART1("\r\n");


            if (vally[3] != -1) {
                if (vally[2] > vally[3]) {
                    for (i = vally[2]; i < 300; i++) {
                        if (RD_buffer[i] > maxRedValue)
                            maxRedValue = RD_buffer[i];
                        if (RD_buffer[i] < minRedValue)
                            minRedValue = RD_buffer[i];
                        if (IR_buffer[i] > maxIRValue) //找到最大最小值
                        {
                            maxIRValue = IR_buffer[i];
                        }
                        if (IR_buffer[i] < minIRValue) {
                            minIRValue = IR_buffer[i];
                        }
                        if (HP_buffer[i] < Pulse_HP_MIN)
                            Pulse_HP_MIN = HP_buffer[i];
                        if (HP_buffer[i] > Pulse_HP_MAX)
                            Pulse_HP_MAX = HP_buffer[i];
                    }
                    for (i = 0; i < vally[3]; i++) {
                        if (RD_buffer[i] > maxRedValue)
                            maxRedValue = RD_buffer[i];
                        if (RD_buffer[i] < minRedValue)
                            minRedValue = RD_buffer[i];
                        if (IR_buffer[i] > maxIRValue) //找到最大最小值，同时记录下他们对应的索引
                        {
                            maxIRValue = IR_buffer[i];
                        }
                        if (IR_buffer[i] < minIRValue) {
                            minIRValue = IR_buffer[i];
                        }
                        if (HP_buffer[i] < Pulse_HP_MIN)
                            Pulse_HP_MIN = HP_buffer[i];
                        if (HP_buffer[i] > Pulse_HP_MAX)
                            Pulse_HP_MAX = HP_buffer[i];
                    }
                } else {
                    for (i = vally[2]; i <= vally[3]; i++) {
                        if (RD_buffer[i] > maxRedValue)
                            maxRedValue = RD_buffer[i];
                        if (RD_buffer[i] < minRedValue)
                            minRedValue = RD_buffer[i];
                        if (IR_buffer[i] > maxIRValue) //找到最大最小值，同时记录下他们对应的索引
                        {
                            maxIRValue = IR_buffer[i];
                        }
                        if (IR_buffer[i] < minIRValue) {
                            minIRValue = IR_buffer[i];
                        }
                        if (HP_buffer[i] < Pulse_HP_MIN)
                            Pulse_HP_MIN = HP_buffer[i];
                        if (HP_buffer[i] > Pulse_HP_MAX)
                            Pulse_HP_MAX = HP_buffer[i];
                    }
                }
            }
            maxIRValueReg = maxIRValue;
            minIRValueReg = minIRValue;
            maxRedValueReg = maxRedValue;
            minRedValueReg = minRedValue;
            Pulse_HP_MINReg = (-1) * Pulse_HP_MAX;
            Pulse_HP_MAXReg = (-1) * Pulse_HP_MIN;
            maxIRValue = 0;
            minIRValue = 1.5;
            maxRedValue = 0;
            minRedValue = 1.5;
            Pulse_HP_MAX = -0x00200000;
            Pulse_HP_MIN = 0x00200000;
            //Call signal functions to calculate heart rate and SpO2         
            if ((pulseStarted == 1)) {//(peakEnable == 1)
                Pulse_rate = calcHeartRate(pulseLength[0], pulseLength[1], pulseLength[2]);
                //            putstringUART1("maxIRValue=\r\n");
                //            SendDecimalismDta(maxIRValue*1000000);
                //            putstringUART1("\r\n");
                //            putstringUART1("minIRValue=\r\n");
                //            SendDecimalismDta(minIRValue*1000000);   
                //            putstringUART1("\r\n");
                //            putstringUART1("maxRedValue=\r\n");
                //            SendDecimalismDta(maxRedValue*1000000);
                //            putstringUART1("\r\n");
                //            putstringUART1("minRedValue=\r\n");
                //            SendDecimalismDta(minRedValue*1000000);
                //            putstringUART1("\r\n");
                SPO2 = calcPulseOx(maxIRValueReg, minIRValueReg, maxRedValueReg, minRedValueReg);
                //Pulse_amp = IR_buffer[ind_max] - IR_buffer[ind_min];


                //低灌注
                /*取消该功能
                if (Pulse_amp <= Amp_threshold)
                    Pulse_weak++;
                else
                    Pulse_weak = 0;
                if (Pulse_weak >= 8) { //此处增加判断是否在测血压，只在血氧时休眠                     
                    if (ReadI2C2Flag == 0 && SPO2orBP == 0) {
                        putstringUART1("Pulse_weak_SLEEP!\r\n");
                        GotoSleep(); //进入休眠
                    }
                }*/
                if (SPO2 >= 100)
                    //SPO2 = SPO2_pre;
                    SPO2 = 100;
                //SPO2_pre = SPO2;
                if (SPO2 != 0) //当血氧不等于0时才开始显示
                {
                    avg_fun();
                }
            }
            //------------------------------------------------------------------        
        }

        if (interval_counter >= (Pulse_Index_Start + 500)) {
            interval_counter = (Pulse_Index_Start + 200);
        }
    }
}

/*
起始数据平均函数
在最开始，因为考虑到所有数据是4个数平均，所以在最开始进行了特殊的处理
以确保一开始的数据偏差不会太大
 */
void avg_fun() {
    //OLED_cnt1++;
    avg_cnt++;
    if (avg_cnt == 1) //采样到第一个数据时，其他3个也为该数据
    {
        Pulse_rate_LP = Pulse_rate; //潘
        Pulse_rate_temp1 = Pulse_rate;

        SpO2_LP = SPO2;
        SpO2_temp1 = SPO2;

    } else if (avg_cnt == 2) //采样到第二个数据时，其他3个数据也为该数据
    {

        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate) >> 1; //潘
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;

        SpO2_LP = (SpO2_temp1 + SPO2) >> 1; //pan
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;

    }//weibin
        /*else if(avg_cnt == 3)			//采样到第三个数据时，最后一个数位最新的这个数据
        {
            Pulse_rate_temp3 = Pulse_rate;
            Pulse_rate_temp2 = Pulse_rate_temp1;
            Pulse_rate_temp1 = Pulse_rate;
            SpO2_temp3 = SPO2;
            SpO2_temp2 = SpO2_temp1;
            SpO2_temp1 = SPO2;	
        }*/
        //pan
    else if (avg_cnt == 3) //采样到第3个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate) / 3; //潘
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SPO2) / 3; //pan
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 4) //采样到第4个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate) >> 2; //潘
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2) >> 2; //pan
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 5) //采样到第5个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate) / 5; //潘
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SPO2) / 5; //pan
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 6) //采样到第6个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate_temp5 + Pulse_rate) / 6; //潘
        Pulse_rate_temp6 = Pulse_rate_temp5;
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SpO2_temp5 + SPO2) / 6; //pan
        SpO2_temp6 = SpO2_temp5;
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//pan
    else if (avg_cnt == 7) //采样到第7个数据时，最后一个数位最新的这个数据
    {
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate_temp5 + Pulse_rate_temp6 + Pulse_rate) / 7; //潘
        Pulse_rate_temp7 = Pulse_rate_temp6;
        Pulse_rate_temp6 = Pulse_rate_temp5;
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SpO2_temp5 + SpO2_temp6 + SPO2) / 7; //pan
        SpO2_temp7 = SpO2_temp6;
        SpO2_temp6 = SpO2_temp5;
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;
    }//weibin
        /*else if(avg_cnt>=4)
            avg_cnt = 4;
         */
        //pan
    else if (avg_cnt >= 8) {

        avg_cnt = 8;
        Pulse_rate_LP = (Pulse_rate_temp1 + Pulse_rate_temp2 + Pulse_rate_temp3 + Pulse_rate_temp4 + Pulse_rate_temp5 + Pulse_rate_temp6 + Pulse_rate_temp7 + Pulse_rate) >> 3; //潘
        Pulse_rate_temp7 = Pulse_rate_temp6;
        Pulse_rate_temp6 = Pulse_rate_temp5;
        Pulse_rate_temp5 = Pulse_rate_temp4;
        Pulse_rate_temp4 = Pulse_rate_temp3;
        Pulse_rate_temp3 = Pulse_rate_temp2;
        Pulse_rate_temp2 = Pulse_rate_temp1;
        Pulse_rate_temp1 = Pulse_rate;
        //SpO2_LP=(SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SPO2)>>2;  //weibin
        SpO2_LP = (SpO2_temp1 + SpO2_temp2 + SpO2_temp3 + SpO2_temp4 + SpO2_temp5 + SpO2_temp6 + SpO2_temp7 + SPO2) >> 3; //pan
        SpO2_temp7 = SpO2_temp6;
        SpO2_temp6 = SpO2_temp5;
        SpO2_temp5 = SpO2_temp4;
        SpO2_temp4 = SpO2_temp3;
        SpO2_temp3 = SpO2_temp2;
        SpO2_temp2 = SpO2_temp1;
        SpO2_temp1 = SPO2;

        /*if (OLED_cnt1 >20)
        {
                        putstringUART1("SpO2_LP\n\r");
                        UART1_Put_Num(SpO2_LP);
                        putstringUART1("\n\r");
        OLED_cnt1 = 0;
        }*/
    }

    //SendDecimalismDta(SPO2);

}

/**
 * 对血氧和脉率的显示值进行滤波处理
 */
void SpO2_Pulse_Display_avg_fun(void) {
    // SportFSM();
    WaveCnt++;
    if (WaveCnt >= 20) //波形1/20s更新一次，数值1s更新一次。
    {
        WaveCnt = 0;
        if (SPO2orBP == 0) {
            if (avg_cnt != 0) {
                if (Pulse_rate_LP == 0) {
                    Pulse_rate_OLED = 0;
                    OLEDSPO2_cnt = 0;
                } else
                    Pulse_rate_OLED = Pulse_rate_LP & 0x000000ff;
                SPO2_OLED = SpO2_LP & 0x000000ff;

                if (OLEDSPO2_cnt == 0) {
                    SPO2_OLED_display = SPO2_OLED;
                    //SPO2_OLED_display = SPO2_OLED + 2;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = Pulse_rate_OLED;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 1) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED) >> 1;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED) >> 1)+2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED) >> 1;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 2) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED) / 3;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED) / 3)+2;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED) / 3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 3) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED) >> 2;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED) >> 2)+2;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED) >> 2;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 4) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED) / 5;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED) / 5)+2;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;


                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED) / 5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 5) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED) / 6;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED) / 6)+2;
                    SPO2_OLED_temp6 = SPO2_OLED_temp5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED_temp5 + Pulse_rate_OLED) / 6;
                    Pulse_rate_OLED_temp6 = Pulse_rate_OLED_temp5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt == 6) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED_temp6 + SPO2_OLED) / 7;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED_temp6 + SPO2_OLED) / 7)+2;
                    SPO2_OLED_temp7 = SPO2_OLED_temp6;
                    SPO2_OLED_temp6 = SPO2_OLED_temp5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    SPO2_OLED_temp1 = SPO2_OLED;

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED_temp5 + Pulse_rate_OLED_temp6 + Pulse_rate_OLED) / 7;
                    Pulse_rate_OLED_temp7 = Pulse_rate_OLED_temp6;
                    Pulse_rate_OLED_temp6 = Pulse_rate_OLED_temp5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt++;
                } else if (OLEDSPO2_cnt >= 7) {
                    SPO2_OLED_display = (SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED_temp6 + SPO2_OLED_temp7 + SPO2_OLED) >> 3;
                    //SPO2_OLED_display = ((SPO2_OLED_temp1 + SPO2_OLED_temp2 + SPO2_OLED_temp3 + SPO2_OLED_temp4 + SPO2_OLED_temp5 + SPO2_OLED_temp6 + SPO2_OLED_temp7 + SPO2_OLED) >> 3) + 3;
                    SPO2_OLED_temp7 = SPO2_OLED_temp6;
                    SPO2_OLED_temp6 = SPO2_OLED_temp5;
                    SPO2_OLED_temp5 = SPO2_OLED_temp4;
                    SPO2_OLED_temp4 = SPO2_OLED_temp3;
                    SPO2_OLED_temp3 = SPO2_OLED_temp2;
                    SPO2_OLED_temp2 = SPO2_OLED_temp1;
                    //SPO2_OLED_temp1 = SPO2_OLED_display;
                    SPO2_OLED_temp1 = SPO2_OLED;
                    /*putstringUART1("\r\n SPO2_OLED_display\r\n");
                    puthexUART1(SPO2_OLED_display);
                    putstringUART1("\r\n");*/

                    Pulse_rate_OLED_display = (Pulse_rate_OLED_temp1 + Pulse_rate_OLED_temp2 + Pulse_rate_OLED_temp3 + Pulse_rate_OLED_temp4 + Pulse_rate_OLED_temp5 + Pulse_rate_OLED_temp6 + Pulse_rate_OLED_temp7 + Pulse_rate_OLED) >> 3;
                    Pulse_rate_OLED_temp7 = Pulse_rate_OLED_temp6;
                    Pulse_rate_OLED_temp6 = Pulse_rate_OLED_temp5;
                    Pulse_rate_OLED_temp5 = Pulse_rate_OLED_temp4;
                    Pulse_rate_OLED_temp4 = Pulse_rate_OLED_temp3;
                    Pulse_rate_OLED_temp3 = Pulse_rate_OLED_temp2;
                    Pulse_rate_OLED_temp2 = Pulse_rate_OLED_temp1;
                    //Pulse_rate_OLED_temp1 = Pulse_rate_OLED_display;
                    Pulse_rate_OLED_temp1 = Pulse_rate_OLED;
                    OLEDSPO2_cnt = 7;
                }
                //如果计算的血氧值大于100；则显示99
                if (SPO2_OLED_display >= 100) {

                    SPO2_OLED_display = 99;
                }
            }
        }
    }
}

/****************************************
函数名：send_uart()
描述：UART发送函数，可将对应的数据通过UART发送，99和88为标志位
 **************************************/
void send_uart(void) {
    /*putcharUART1(99);
    putcharUART1(88);
    p_RD = (unsigned char*)(&abc_test);
    p_IR = (unsigned char*)(&abc_test_dc);*/



    /*	putcharUART1(Pulse_HP);
        putcharUART1(Pulse_HP>>8);
        putcharUART1(Pulse_HP>>16);
        putcharUART1(Pulse_HP>>24);
     */
    /*putcharUART1(*p_RD);
    putcharUART1(*(p_RD+1));
    putcharUART1(*(p_RD+2));
    putcharUART1(*(p_RD+3));

    putcharUART1(*p_IR);
    putcharUART1(*(p_IR+1));
    putcharUART1(*(p_IR+2));
    putcharUART1(*(p_IR+3));

    putcharUART1(Pulse_HP_Total_avg);
    putcharUART1(Pulse_HP_Total_avg>>8);
    putcharUART1(Pulse_HP_Total_avg>>16);
    putcharUART1(Pulse_HP_Total_avg>>24);*/

    /*
        putcharUART1(SPO2);
        putcharUART1(SPO2>>8);

        putcharUART1(Pulse_rate);
        putcharUART1(Pulse_rate>>8);
        putcharUART1(Pulse_rate>>16);
        putcharUART1(Pulse_rate>>24);

        putcharUART1(currentValue);
        putcharUART1(currentValue>>8);
        putcharUART1(currentValue>>16);
        putcharUART1(currentValue>>24);
     */

    //putcharUART1(SPO2);
    //putcharUART1(SPO2>>8);
}
