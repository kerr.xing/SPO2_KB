/* 
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 作者              日期      
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 魏彬       		2014年5月22日	
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 描述
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 对管脚控制和UART模块进行了初始化。
 **********************************************************************/

#include "p32mx150f128b.h"
#include "extern_func.h"
#include <plib.h>
#include "Port_init.h"

void initPORTs(void);

/****************************************
函数名：initPORTs(void)
描述：用于初始化io管脚的配置和分配
 **************************************/
void initPORTs(void) {
    //	ANSELASET = 0x0003;		//将端口都设置为数字IO模式
    ANSELACLR = 0x0007;
    ANSELBCLR = 0xF01F; //kebin ,INT4
    //	ANSELCCLR = 0x000F;			//SW123，reset_bt，led为数字
    //	ANSELCSET = 0x0004;			//Vsen为模拟
    //***********LEDControl************//
    RST_AFE_TRIS = 0;
    STE_TRIS = 0;
    SIMO_TRIS = 0;
    ADC_RDY_TRIS = 1;
    SOMI_TRIS = 1;
    PWD_AFE_TRIS = 0;
    SPI_CLK_TRIS = 0;
    //***************OLED***************//
    DC_TRIS = 0;
    DC = 0;
    OLED_RST_TRIS = 0;
    OLED_SDIN_TRIS = 0;
    OLED_SCLK_TRIS = 0;
    //**************ACM****************//
    ACM_INT1_TRIS = 1;
    //ODCBSET = 0X0010; //RB4开漏
    //	ACM_INT1=1;
    //************SWITCH&Vsen***************//
    SW1_TRIS = 1;
    CLK32K_TRIS = 1;
    //*******************************//
    IO_RA2_TRIS = 0;
    IO_RA2 = 0;
    //************UART/I2C*************//
    //	TxD_SCL_TRIS = 0;
    //	Rxd_SDA_TRIS = 1;
    //************PPS****************//
    CFGCONbits.IOLOCK = 0;
    SDI1Rbits.SDI1R = 1; //SDI1映射到PRB5		input
    RPB11Rbits.RPB11R = 3; //SDO1映射到RPB11		output
    //	RPC7Rbits.RPC7R = 3;		//SS1映射到PRC7
    RPA1Rbits.RPA1R = 4; //SDO2映射到RPA1	output

    INT1Rbits.INT1R = 0; //把INT1映射到RPA3   SW
    INT4Rbits.INT4R = 2; //把INT4映射到RPB4 ACM_INT1
    //	INT3Rbits.INT3R = 7;		//把INT3映射到RPA9 SW2
    CFGCONbits.IOLOCK = 1;
}